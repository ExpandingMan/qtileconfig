#!/bin/bash
$HOME/.screenlayout/default.sh
$HOME/.local/bin/bgrandom &
picom --config $HOME/.config/picom/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/bin/gnome-keyring-daemon --start &
nm-applet &
canberra-gtk-play -i service-login &
