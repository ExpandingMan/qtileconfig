from typing import List  # must be imported because god knows why
import os
import re
import subprocess

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from utils import *
from constants import *
from workspaces import *
from keys import *
from layouts import *
from widgets import *
from screens import *


#=============================================================================================================#
#  startup
#=============================================================================================================#
@hook.subscribe.startup_once
def initialize():
    subprocess.run([os.path.expanduser("~/.config/qtile/startup.sh")])
#=============================================================================================================#

#=============================================================================================================#
#  config
#=============================================================================================================#
groups = defaultgroups()
keys = keymap(groups)
layouts = defaultlayouts()

widget_defaults = dict(
        font="JuliaMono",
        fontsize=14,
        padding=4,
        background=colors["background"],
        foreground=colors["foreground"],
        highlight_color=[colors["selection"], colors["comment"]],
        border_color=colors["selection"],
        border=colors["comment"],
        rounded=False,
        )
extension_defaults = widget_defaults.copy()

screens = defaultscreens()

# Drag floating layouts. (2 and 3 are swapped for some reason)
mouse = [
        Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
        Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
        Click([mod], "Button2", lazy.window.toggle_floating()),
        ]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(
        **layout_theme,
        float_rules=[
            # Run the utility of `xprop` to see the wm class and name of an X client.
            *layout.Floating.default_float_rules,
            Match(wm_type="Firefox"),
            ]
        )
auto_fullscreen = True
focus_on_window_activation = "never"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wmname = "qtile"
