import os
import re
import subprocess

def get_nscreens():
    xr = subprocess.check_output('xrandr --query | grep " connected"', shell=True).decode().split('\n')
    monitors = len(xr) - 1 if len(xr) > 2 else len(xr)
    return monitors

def get_network_interface():
    n = subprocess.check_output("ip -o -4 route show to default | awk '/dev .*/{print $5}'",
            shell=True)
    n = n.decode().strip()
    return n

def haswifi():
    """
    Attempt to determine if the system has wifi.  This is a bit hackish at the moment.
    """
    nwk = get_network_interface()
    return nwk.startswith("w")

def styledtext(s, **kw):
    """
    Style the string `s` ising pango markup.  See
    [here](https://www.chordpro.org/chordpro/pango_markup/) for a list of available options.
    """
    if len(kw) == 0:
        return s
    o = "<span"
    for (k, v) in kw.items():
        o += f' {k}="{v}"'
    o += ">" + s + "</span>"
    return o

def hasbattery():
    """
    Attempt to determine if the system has a battery.
    """
    fs = os.listdir("/sys/class/power_supply/")
    for f in fs:
        if f.lower().startswith("bat"):
            return True
    return False

def discoverterm():
    """
    Attempt to infer which is the correct terminal to use.
    Note that using fish for this is buggy, so you need a python function to discover it.
    """
    if os.path.exists("/usr/bin/rio"):
        return "rio"
    elif os.path.exists("/usr/bin/wezterm"):
        return "wezterm"
    elif os.path.exists("/usr/bin/kitty"):
        return "kitty"
    elif os.path.exists("/usr/bin/alacritty"):
        return "alacritty"
    elif os.path.exists(os.path.expanduser("~/.cargo/bin/alacritty")):
        return "alacritty"
    elif os.path.exists("/usr/bin/urxvt"):
        return "urxvt"
    else:
        # maybe try global env var
        return os.environ.get("TERMINAL")
