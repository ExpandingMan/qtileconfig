from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from utils import *
from layouts import *


def defaultgroups():
    gps = [Group(i) for i in "1234567890"]
    # top group
    gps.append(Group("top", label="", matches=[Match(wm_instance_class="btop")]))
    gps.append(Group("browser", label="",
        matches=[Match(wm_class=re.compile("[Ff]irefox")), Match(wm_class=re.compile("[Bb]rave"))],
        layout="max"))
    gps.append(Group("games", label="", matches=[Match(wm_class=re.compile("[Ss]team"))],
        layouts=steamlayouts()))
    gps.append(Group("video", label="𝔙", matches=[Match(wm_class=re.compile("[Ff]ree[Tt]ube"))], layout="max"))
    return gps

