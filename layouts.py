import os
import re
import subprocess

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from utils import *
from constants import *

# we hide the border in this theme for consistency with Columns
def make_max_theme(theme):
    o = theme.copy()
    o["border_width"] = 0
    return o

def defaultlayouts(colors=colors):
    return [
            layout.Columns(**layout_theme),
            layout.Zoomy(**layout_theme),
            layout.Max(**make_max_theme(layout_theme)),
            layout.Floating(**layout_theme),
            ]

def steamlayouts(colors=colors):
    return [layout.Columns(**steam_layout_theme),
            layout.Max(**make_max_theme(layout_theme)),
            layout.Floating(**layout_theme),
            ]
