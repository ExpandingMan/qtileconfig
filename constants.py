from utils import *

mod = "mod4"

# uses fish function to guess and run correct terminal
terminal = discoverterm()

colors = {"background": "#282a36",
        "black": "#000000",
        "selection": "#44475a",
        "foreground": "#f8f8f2",
        "comment": "#6272a4",
        "cyan": "#8be9fd",
        "green": "#50fa7b",
        "orange": "#ffb86c",
        "pink": "#ff79c6",
        "purple": "#bd93f9",
        "red": "#ff5555",
        "yellow": "#f1fa8c",
        }

layout_theme = {"border_width": 1,
        "margin": 0,
        "border_focus": colors["comment"],
        "border_normal": colors["black"],
        }

steam_layout_theme = {
        "border_width": 1,
        "margin_on_single": [64, 64, 64, 1440],
        "margin": [64, 64, 64, 64],
        "border_focus": colors["comment"],
        "border_normal": colors["black"],
        }

nscreens = get_nscreens()

moonlander_url = "https://configure.zsa.io/moonlander/layouts/NpwY6/latest/"
