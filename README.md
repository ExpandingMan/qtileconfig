# `qtileconfig`

Configuration for [`qtile`](http://www.qtile.org/).

The configuration itself is in `config.py` which imports functions from other files.

Currently the whole thing is a mess all over a single directory instead of keeping the source files
separate becuase Python's `module`/`import` system is a horrendous dumpster fire that I can't be
bothered with.


## Requirements
- `qtile`: obviously
- `iwlib`: `pip install iwlib` for wifi.
- `ptpython`: only for the `repl.py` I have here.

## Useful config examples
- https://github.com/justinesmithies/qtile-x-dotfiles/blob/master/.config/qtile/config.py
- https://gitlab.com/LongerHV/.dotfiles/blob/master/.config/qtile/config.py

