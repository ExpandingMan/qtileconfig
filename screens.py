import os
import re
import subprocess

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.log_utils import logger

from utils import *
from constants import *
from widgets import *


def defaultbar(colors=colors):
    return bar.Bar(
            [
                widget.CurrentLayoutIcon(scale=0.6),
                defaultsep(),
                defaultgroupbox(),
                defaultsep(),
                widget.Chord(background=colors["orange"], foreground=colors["background"]),
                defaultwindowname(),
                defaultprompt(),
                defaultclock(),
                widget.Spacer(length=12),
                *defaultwifi(),
                widget.Spacer(length=12),
                *defaultcpugraph(),
                widget.Spacer(length=12),
                *defaultmemorygraph(),
                widget.Spacer(length=12),
                defaultvolume(),
                *defaultbattery(),
                ],
            28, background=colors["black"], margin=0, opacity=0.75,
            )

def defaultscreens(n=nscreens, colors=colors):
    return [Screen(bottom=defaultbar(colors)) for i in range(n)]

